from utils import *

prev = {'name': {"$regex": "^A", "$options": 'i'}}  # query for the documents to update
next = {"$set": {"age": 31, "Address.city": "Mumbai"}}

# Update a single document
# collection.update(prev, next)
# allDocs = collection.find({'name': {"$regex": "^A", "$options": 'i'}})
# print(allDocs.count())
# for a_doc in allDocs:
#     print(a_doc, end=";")

# update all the document matching the query
updated = collection.update_many(prev, next)
allDocs = collection.find({'name': {"$regex": "^A", "$options": 'i'}})
print(allDocs.count())
print(updated.modified_count)
for a_doc in allDocs:
    print(a_doc, end=";")
