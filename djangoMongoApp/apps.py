from django.apps import AppConfig


class DjangomongoappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'djangoMongoApp'
