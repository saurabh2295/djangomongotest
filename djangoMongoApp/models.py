from django.db import models
from djongo import models


# Create your models here.
class Post(models.Model):
    _id = models.ObjectIdField()
    post_title = models.CharField(max_length=255)
    description = models.TextField()
    comment = models.JSONField()
    user_details = models.JSONField()
    tags = models.JSONField()
    objects = models.DjongoManager()
