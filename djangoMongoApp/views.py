from django.shortcuts import render
from bson import ObjectId
from django.http import JsonResponse
from .models import Post
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
import json


# Create your views here.
@csrf_exempt
def add_post(request):
    payload = request.POST
    print(payload)
    comment = payload.get("comment").split(",")
    tags = payload.get("tags").split(",")
    user_details = {"first_name": payload.get("first_name"), "last_name": payload.get("last_name")}
    print(user_details)
    post = Post(post_title=payload.get("title"), description=payload.get("description"),
                comment=comment, tags=tags, user_details=user_details)
    post.save()
    return JsonResponse({"message": "Post Added"})


def update_post(request, id):
    pass


def delete_post(request, id):
    pass


def read_post(request, id):
    post = Post.objects.get(_id=ObjectId(id))
    print(hasattr(post.user_details, "first_name"))
    if not ("first_name" in post.user_details):
        post.user_details["first_name"] = ""
    if not ("last_name" in post.user_details):
        post.user_details["last_name"] = ""
    return JsonResponse({
        "firstName": post.user_details["first_name"], "lastName": post.user_details["last_name"],
        "Title": post.post_title
    })


def read_post_all(request):
    posts = Post.objects.all()
    data = json.loads(serializers.serialize("json", posts))
    print(data)
    print(len(data))
    return JsonResponse(data, safe=False)
