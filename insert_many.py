from utils import *

list_of_dicts = [
    {"name": "ABC", "age": 25, "Address": {"city": "C1", "state": "S1", "pinCode": 111222}},
    {"name": "ACB", "age": 26, "Address": {"city": "C2", "state": "S2", "pinCode": 111223}},
    {"name": "BCA", "age": 27, "Address": {"city": "C3", "state": "S3", "pinCode": 111224}},
    {"name": "CBA", "age": 28, "Address": {"city": "C4", "state": "S4", "pinCode": 111225}}
]
collection.insert_many(list_of_dicts)
print("Inserted more than one documents")

