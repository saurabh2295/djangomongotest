from utils import *
from bson import ObjectId

singleDoc = collection.find_one({'name': {"$regex": "^A"}})
print(singleDoc)
allDocs = collection.find({'name': {"$regex": "^A", "$options": 'i'}})
print(allDocs.count())
for a_doc in allDocs:
    print(a_doc, end=";")

# find by id
doc = collection.find({"_id": ObjectId("61440d1192af76d050210f49")})
print(doc.next())