from utils import *

records_to_delete = {'name': {"$regex": "^A", "$options": 'i'}}  # query for the documents to delete
print(collection.find(records_to_delete).count())
for a_doc in allDocs:
    print(a_doc, end=";")

# Delete a single document
# collection.delete_one(records_to_delete)


# delete all the document matching the query
deleted = collection.delete_many(records_to_delete)
allDocs = collection.find(records_to_delete)
print(deleted.deleted_count)
print(allDocs.count())
for a_doc in allDocs:
    print(a_doc, end=";")
